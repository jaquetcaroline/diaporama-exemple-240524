# Les facteurs d'ambiance

----
## Extérieurs

-Attirer le regard

-Donner envie de rentrer

![Devanture magasin](https://france3-regions.francetvinfo.fr/image/EcbjFENmXVkzjs9a3Lpo1-Ik2ag/600x400/regions/2023/02/08/63e3857aa70cc_maxnewsworldfive877070.jpg)

----
## Intérieurs

- Créer une ambiance confortable.

- Donner envie de rester.

----
### Visuels
- Théâtralisation 
- Mise en scène produit
- Décor
- Propreté
- Luminosité

----
### Olfactifs / Gustatifs

1 Diffuseur

2 Dégustation

----
### Sonores
- Ambiance musicale

- SOns spécifiques

---
# La mise en scène d'une vitrine


<iframe width="560" height="315" src="https://www.youtube.com/embed/ZWDlKrbm-F4?si=9kkfI6Bw6hapF4np" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

---
# Mettre un produit en avant


<iframe width="560" height="315" src="https://www.youtube.com/embed/ooS6jxNxqec?si=i78a06I6AKIr6ZMQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

